# Компаненты #

***ajax.wrapper.list***

* ajax враппер для списков, форм
* позволяет последовательно загружать элементы с других компанентов (с пагинацией)
* фильтр по параметру, для списка
![ajax.wrapper.list.png](https://bitbucket.org/repo/kMkxLA7/images/2632180476-ajax.wrapper.list.png)