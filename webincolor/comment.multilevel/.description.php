<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("AV_CM_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("AV_CM_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/photo_detail.gif",
	"CACHE_PATH" => "Y",
	"SORT" => 300,
	"PATH" => array(
			"ID" => "alexvaleev",
			"NAME" => GetMessage("AV"),
			"SORT" => 20,
			"CHILD" => array(
				"ID" => "av_comments",
				"NAME" => GetMessage("AV_CM")
			),

	),
);

?>