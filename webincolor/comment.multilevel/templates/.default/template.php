<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="comments">
	<div class="top-coments">
		<h2>Мнения</h2>
		<div class="top-icon"></div>
		<div class="border-4-icon">
		</div>
        <a class="option" onclick="if(portalIsAuthCall()) { showComment('0'); $(this).closest('.comments').find('.send-massedg').toggle();};return false"; href="javascript:void(0)">Есть мнение?</a>
	</div>
<?
$APPLICATION->IncludeComponent(
	"bitrix:blog.post.comment",
	"main",
	Array(
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"COMMENTS_COUNT" => $arParams["COMMENTS_COUNT"],
		"PATH_TO_SMILE" => $arParams["PATH_TO_SMILE"],
		"BLOG_URL" => $arParams["BLOG_URL"],
		"ID" => $arResult["COMMENT_ID"],
		"SHOW_RATING" => $arParams["SHOW_RATING"],
		"PATH_TO_USER" => $arParams["PATH_TO_USER"],
		"PATH_TO_BLOG" => $arParams["PATH_TO_BLOG"],
		"PATH_TO_POST" => $arResult["ELEMENT"]["~DETAIL_PAGE_URL"],
		"SIMPLE_COMMENT" => "Y"
	),
	$component,
	array("HIDE_ICONS" => "Y"));
?>
</div>