<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="rat rat-item">
    <div class="vote-item">
        <? if ($arResult['USER']) { ?>
            <div class="icon-rate"></div>
            <span class="my_evaluation">моя оценка:
                <span class="bigtxt"><?= $arResult['USER']['ANSWER'] ?></span> из 10
            </span>
            <a class="vote vote-show">Переголосовать</a>
        <? } else { ?>
            <a class="vote vote-show">Голосовать</a>
        <? } ?>
    </div>
    <form method="post" action="<?= POST_FORM_ACTION_URI ?>" class="vote-form hidden <?= $arParams['FORM_SELECT'] ?>">
        <select name="rating" id="iblock_vote_<?= $arResult['ID'] ?>">
            <? if (empty($arResult['USER'])) { ?>
                <option value="0">-</option>
            <? } ?>
            <? for ($i = 1; $i <= 10; $i++) { ?>
                <option value="<?= $i ?>" <? if ($arResult['USER'] and $i == $arResult['USER']['ANSWER']) { ?>selected="selected"<? } ?>><?= $i ?></option>
            <? } ?>
        </select>
        <? echo bitrix_sessid_post(); ?>
        <input type="hidden" name="AJAX_CALL" value="Y" />
        <input type="hidden" name="back_page" value="<?= $arResult["BACK_PAGE_URL"] ?>" />
        <input type="hidden" name="vote_id" value="<?= $arResult["ID"] ?>" />
        <input type="hidden" name="vote" value="<?= GetMessage("T_IBLOCK_VOTE_BUTTON") ?>" />
        <input type="submit" name="submit" class="vote" value="<?= GetMessage("T_IBLOCK_VOTE_BUTTON") ?>" />
    </form>
</div>