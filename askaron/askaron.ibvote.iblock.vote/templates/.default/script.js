function initVoteRating() {
//число из data трибута
    var data_number = parseFloat($('.img-mask-off').data('rating'));

    //округляем в меньшую сторону
    var round_number = Math.floor(data_number);

    //margin-left
    var mrg_lf = (data_number - round_number);
    mrg_lf = (mrg_lf * 15).toFixed(1);
//    console.log(mrg_lf + ' margin-left');

    //шинина блока
    var width_block = 15 - mrg_lf;
//    console.log(width_block + ' width');

    $('.user-rating-js .rating-block .rating-btn').each(function () {
        if ($(this).index() == round_number) {
            $(this).children().addClass('chunk').css({
                'width': width_block + 'px',
                'margin-left': mrg_lf + 'px',
            });
        }
        if ($(this).index() < round_number) {
            $(this).addClass('hover');
        }
    });

    $(document).on('mouseleave', '.user-rating-js .rating-block .rating-btn', function () {
        $('.user-rating-js .rating-block .rating-btn').each(function () {
            if ($('.pre_active').length == 0) {
                if ($(this).index() < round_number) {
                    $(this).addClass('hover');
                    $('.user-rating-js .chunk').removeClass('normal');

                } else {
                    $(this).removeClass('hover');

                }
            } else {
                $(this).removeClass('hover');
            }
        });
    });
}

$(function () {

    $.get('/local/components/askaron/askaron.ibvote.iblock.vote/templates/.default/ajax.php', {element: $('.img-mask-off').data('element')}, function (res) {
        if (res.success && res.is) {
            $('.img-mask-off').data('user-vote', res.is);
        }
    }, 'json');

    $(document).on('click', '.user-rating-js .vote', function () {
//        console.log($(this).closest('.user-rating-js').find('.vote-form input[name="rating"]').val());
        if ($(this).closest('.user-rating-js').find('.vote-form input[name="rating"]').val()) {
            $('.user-rating-js').find('.vote-form').submit();
        }
    });

// РЕЙТИНГ
    $(document).on('mouseenter', '.user-rating-js .rating-block .rating-btn', function () {
        var index = $(this).index();
        $('.user-rating-js .rating-block .rating-btn').each(function () {
            if ($(this).index() <= index) {
                $(this).addClass('hover');
                if (index < $('.chunk').parents().index()) {
                    $('.user-rating-js .chunk').addClass('normal');
                }
            } else {
                $(this).removeClass('hover');
            }

        });
    });

    $(document).on('click', '.user-rating-js .rating-block .rating-btn', function () {
        var index = parseInt($(this).index()) + 1;
        var count = parseInt($('.img-mask-off').data('vote-count'));
        var sum = parseInt($('.img-mask-off').data('vote-sum'));
        var vote = parseInt($('.img-mask-off').data('user-vote'));
        if(vote) {
            sum += index - vote;
        } else {
            sum += index;
            count++;
        }
        var text = count;
        switch (count) {
            case 1:
                text += ' проголосовавший';
                break;

            default:
                text += ' проголосовавших';
                break;
        }
        $('.user-rating-js .clicked-users').html(text);
        $('.user-rating-js .all-rating').html( Math.round(sum/count*10)/10);

        $(this).closest('.user-rating-js').find('.vote-form input[name="rating"]').val(index);
        $(this).addClass('active').siblings().removeClass('active');
        $('.user-rating-js .rating-block .rating-btn').each(function () {
            if ($(this).index() < index) {
                $(this).addClass('pre_active');
            } else {
                $(this).removeClass('pre_active');
            }
        });
    });

// РЕЙТИНГ end
});