<?

use Bitrix\Main\Application,
    Bitrix\Main\Loader;

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
if (Loader::includeModule('askaron.ibvote')) {
    $request = Application::getInstance()->getContext()->getRequest();
    if($request->get('element')) {
        $event = new CAskaronIbvoteEvent;
        if($arItem = $event->getByUser($request->get('element'))) {
            $arResult['is'] = $arItem['ANSWER'];
        }
    }
}
$arResult['success'] = true;
echo json_encode($arResult);
