<?

use Bitrix\Main\Loader,
    Igromafia\Game\Model\VoteTable;


$arResult['RATING'] = round($arResult['PROPERTY_RATING_VALUE'], 1);
$arResult['VOTE_COUNT'] = $arResult['PROPERTY_VOTE_COUNT_VALUE'];
$arResult['VOTE_SUM'] = $arResult['PROPERTY_VOTE_SUM_VALUE'];
if (empty($arResult['RATING'])) {
    $arResult['RATING'] = $arItem['PROPERTY_RATINGIGROMAFII_VALUE'];
}
switch ($arResult['VOTE_COUNT']) {
    case 1:
        $arResult['VOTE_COUNT_TEXT'] = 'проголосовавший';
        break;

    default:
        $arResult['VOTE_COUNT_TEXT'] = 'проголосовавших';
        break;
}
if (Loader::includeModule('igromafia.game')) {
    $arResult['IMG'] = Igromafia\Game\Image::resize($arResult['PREVIEW_PICTURE'] ?: $arResult['DETAIL_PICTURE'], 300, 368);
}
