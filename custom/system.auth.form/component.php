<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

//if ($USER->IsAuthorized() and 0) {
//    if (isset($arParams['IS_AJAX'])) {
//        $arResult['AUTHORIZED'] = 'ok';
//        echo json_encode($arResult);
//        exit;
//    } else {
//        LocalRedirect($arParams['PROFILE_URL']);
//    }
//}

$arResult["ERRORS"] = "";
$arResult["MESSAGE"] = "";


if (isset($_POST["pass_sms"])) {
    if (empty($_POST["USER_SMS_PASS"])) {
        $arResult["ERRORS"] = "<p class='error'>Не указан пароль с телефона!</p>";
    } else if (isset($_SESSION['duoweb.gpsms.user']) and isset($_SESSION['duoweb.gpsms.time']) and isset($_SESSION['duoweb.gpsms.time']) and $_SESSION['duoweb.gpsms.time'] > time()) {
        if ($_SESSION['duoweb.gpsms.password'] === md5($_POST["USER_SMS_PASS"])) {
            $USER->Authorize($_SESSION['duoweb.gpsms.user']);
            if ($USER->IsAuthorized()) {
                $arResult['AUTHORIZED'] = 'ok';
            } else {
                $arResult["ERRORS"] = "<p class='error'>Неизвестная ошибка.</p>";
            }
            unset($_SESSION['duoweb.gpsms.user']);
        } else {
            $arResult["ERRORS"] = "<p class='error'>смс пароль указан не верно.</p>";
        }
    } else {
        $arResult["ERRORS"] = "<p class='error'>Время сессии истекло.</p>";
    }
}

if (isset($_POST['authorized'])) {
    if (empty($_POST["USER_LOGIN"]) or empty($_POST["USER_PASSWORD"])) {
        $arResult["ERRORS"] = "<p class='error'>Не указан логин или пароль.</p>";
    } else {
        $arAuthResult = $USER->Login($_POST["USER_LOGIN"], $_POST["USER_PASSWORD"], "Y");
//        pre($arAuthResult, $_POST);
        if ($arAuthResult === true) {
            $arUser = $USER->GetById($USER->GetId())->Fetch();
            if ($arUser['UF_PHONE_AUTORIZE']) {
                $arResult['AUTHORIZED'] = 'ok';
            } else {
                $UF_PHONE = str_replace(array(' ', '-', "+", "\t"), '', trim($arUser['UF_PHONE']));
                $_SESSION['duoweb.gpsms.user'] = $USER->GetId();
                $USER->Logout();

                $arResult["MESSAGE"] = "
    <p class='message'>
        Пароль " . (isset($_POST["pass_sms"]) ? 'повторно' : '') . " выслан на телефон, указанный в настройках профиля пользователя. Обратите внимание, что время жизни пароля - 3 минуты.
    </p>";
                $pass = mt_rand(100000, 999999);
                $MyOrderCall = new COrderCall;

                $GPSMS_sender_phone = COption::GetOptionString("duoweb.gpsms", "user_phone");
                $array_phones = COption::GetOptionString("duoweb.gpsms", "list_phones");
                $finally_array_phones = $array_phones . "," . $GPSMS_sender_phone;

                $login = COption::GetOptionString("duoweb.gpsms", "login");
                $password = COption::GetOptionString("duoweb.gpsms", "password");
                $new_user_check = COption::GetOptionString("duoweb.gpsms", "new_user_check");
                $signature_select = COption::GetOptionString("duoweb.gpsms", "signature_select");
                if ($new_user_check == "Y" and ! empty($UF_PHONE)) {
                    $hash = $MyOrderCall->Login($login, $password);
                    $hash = filter_var($hash, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
                    $hash = substr($hash, 2);
                    $signature = $MyOrderCall->Signatures_Get($hash);
                    $signature = filter_var($signature, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
                    $MyOrderCall->SmsDistribution($UF_PHONE, $pass, $signature_select, $hash);
                }

                $_SESSION['duoweb.gpsms.password'] = md5($pass);
                $_SESSION['duoweb.gpsms.time'] = time() + 3600;
//            $arResult["pass"] = $pass;
            }
        } else {
            if (isset($arAuthResult['MESSAGE'])) {
                $arResult["ERRORS"] = "<p class='error'>" . $arAuthResult['MESSAGE'] . "</p>";
            } else {
                $arResult["ERRORS"] = "<p class='error'>Ошибка авторизации.</p>";
            }
        }
    }
}

if (isset($arParams['IS_AJAX'])) {
    echo json_encode($arResult);
    exit;
}

$this->IncludeComponentTemplate();
