<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $APPLICATION->IncludeComponent(
        "duoweb.gpsms:script", "isAuth", Array(
    "IS-AUTH_URL" => SITE_DIR . "personal/",
    "NO-AUTH_URL" => ""
        ), false
); ?>
<script language = 'javascript'>
    var jsSystemAuthFormPath = '<?= $templateFolder ?>';
    var jsSystemAuthFormPROFILE_URL = '<?= $arParams['PROFILE_URL'] ?>';
</script>
<div class="box" id="login_form_sms" style="display: block;">
    <div class="box_title">Вход</div>
    <div class="box_content">

        <div class="login_form_error">
            <? if ($arResult["ERRORS"] != ""): ?>
                <?= $arResult["ERRORS"] ?>
            <? endif; ?>
            <? if ($arResult["MESSAGE"] != ""): ?>
                <?= $arResult["MESSAGE"] ?>
            <? endif; ?>
        </div>

        <form name="authorized" method="post" target="_top" action="">
            <input type="hidden" name="authorized" value="1" />
            <input type="hidden" name="is_sms" value="0" />
            <div class="box_form_field field_login">
                <input type="text" class="inp_text" name="USER_LOGIN" placeholder-text="Логин (e-mail)">
            </div>
            <div class="box_form_field field_password">
                <input type="password" class="inp_text" name="USER_PASSWORD" placeholder-text="Пароль" real_type="password">
            </div>
            <div class="box_form_field field_sms tHide">
                <input type="text" class="inp_text" name="USER_SMS_PASS" placeholder-text="Временный пароль, пришедший по смс" real_type="text">
            </div>
            <div class="passwordRequest tHide"><a>Повторить запрос</a></div>
            <div class="box_controlls">
                <a href="" class="forgot_password_link">Забыли пароль ?</a>
                <button class="btn_form_submit" name="Login">Войти</button>
            </div>
        </form>

<!--        <form name="password_sms" method="post" class="password_sms tHide" target="_top" action="">
            <input type="submit" name="pass_sms"  class="adm-btn-save" value="Получить пароль на телефон администратора" />
        </form>-->
    </div>
</div>
