$(function () {

    var passwordRequest = function () {
        var form = $('#login_form_sms form[name="authorized"]');
        $.post(jsSystemAuthFormPath + '/ajax.php', form.serialize(), function (data) {
            if (data.AUTHORIZED && data.AUTHORIZED == 'ok') {
                document.location.href = jsSystemAuthFormPROFILE_URL;
                return true;
            }
            if (data.ERRORS) {
                $('#login_form_sms div.login_form_error').html(data.ERRORS);
            } else if (data.MESSAGE) {
                $('#login_form_sms div.login_form_error').html(data.MESSAGE);
                form.find('.field_login, .field_password').hide();
                form.find('.field_sms, .passwordRequest').show();
                form.find('input[name="is_sms"]').val(1);
            }
            console.log(data);
        }, 'json');
    };

    $('#login_form_sms form[name="authorized"] .passwordRequest').click(function () {
        passwordRequest();
    });

    $('#login_form_sms form[name="authorized"]').submit(function () {
        var form = $('#login_form_sms form[name="authorized"]');
        if (form.find('input[name="is_sms"]').val() == 1) {
            $.post(jsSystemAuthFormPath + '/ajax.php', {pass_sms: true, USER_SMS_PASS: form.find('input[name="USER_SMS_PASS"]').val()}, function (data) {
                if (data.AUTHORIZED && data.AUTHORIZED == 'ok') {
                    document.location.href = jsSystemAuthFormPROFILE_URL;
                    return true;
                }
                if (data.ERRORS) {
                    $('#login_form_sms div.login_form_error').html(data.ERRORS);
                } else {
                    $('#login_form_sms div.login_form_error').html('<p class="error">Возникла ошибка, повторите запрос попоже</p>');
                }
                console.log(data.MESSAGE);
            }, 'json');

        } else {
            passwordRequest();
        }
        return false;
    });

});











