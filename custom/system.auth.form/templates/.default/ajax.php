<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent(
        "duoweb.gpsms:system.auth.form", "", Array(
    "IS_AJAX" => "Y",
    "REGISTER_URL" => "",
    "FORGOT_PASSWORD_URL" => "",
    "PROFILE_URL" => "/personal/",
    "SHOW_ERRORS" => "Y"
        )
);
