<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


if (isset($_POST['ajax']) and $_POST['ajax'] === 'subscribe') {

    if (!function_exists('GetMessageMailchimp')) {

        function GetMessageMailchimp($msd, $msg2 = '', $isSuccess = false) {
            $msd = GetMessage($msd) . $msg2;
            echo ($isSuccess ? '<span class="subscribe-succes">' : '<span class="subscribe-error">') . $msd . '</span>';
            exit;
        }

    }
    if (!CModule::IncludeModule("subscribe")) {
        GetMessageMailchimp('SUBSCRIBE_ERROR_SUBSCRIBE');
    }
    // Validation
    if (!$_POST['email']) {
        GetMessageMailchimp('SUBSCRIBE_NO_EMAIL');
    }

    if (!check_email($_POST['email'])) {
        GetMessageMailchimp("SUBSCRIBE_EMAIL_INVALID");
    }

    $aPostRub = array(1);
    $subscr = CSubscription::GetList(
                    array("ID" => "ASC"), array("RUBRIC" => $aPostRub, "EMAIL" => $_POST['email'])
    );
    if (($subscr_arr = $subscr->Fetch())) {
        if($subscr_arr['CONFIRMED']=='Y' and $subscr_arr['ACTIVE']=='Y') {
            GetMessageMailchimp('SUBSCRIBE_IS_SUCCESS', '', true);
        } else {
            CSubscription::Delete($subscr_arr['ID']);
        }

    }

    $arFields = Array(
        "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
        "FORMAT" => "html",
        "EMAIL" => $_POST['email'],
        "ACTIVE" => "Y",
        "RUB_ID" => $aPostRub
    );
    $subscr = new CSubscription;
    $ID = $subscr->Add($arFields);
    if ($ID > 0) {
        GetMessageMailchimp('SUBSCRIBE_SUCCESS', '', true);
    } else {
        GetMessageMailchimp('SUBSCRIBE_ERROR_SUBSCRIBE', $error);
    }
    exit;
}
$arResult['RUN_URL'] = str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__) . '/run.php';
$this->IncludeComponentTemplate();
?>