<?
$jsParams = array(
    "AJAX" => $arResult["AJAX"]
);
Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
?>
<script>
    var PersonalInfo = new jsPortalPersonalInfo(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
</script>