<div class="login">
    <? if ($arResult['IS_USER']) { ?>
        <div class="login_top hidden-xs">
            <a class="hd-link my_name-txt" href="/personal/"><?=$arResult['USER']['NAME'] ?></a>
            <span class="stick">|</span>
            <a class="hd-link personal-info-logout">Выйти</a>
        </div>
        <a class="hd-login_bot hidden-xs" href="/personal/">Личный кабинет</a>
    <? } else { ?>
        <div class="login_top hidden-xs">
            <a class="hd-link enter_hd_btn">Войти</a>
            <span class="stick">|</span>
            <a class="hd-link family_hd_btn">Вступить в семью</a>
        </div>
        <a class="hd-login_bot hidden-xs enter_hd_btn-personal">Личный кабинет</a>
    <? } ?>
</div>