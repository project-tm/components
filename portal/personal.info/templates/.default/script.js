$(function () {

    $('.personal-info-logout').on('click', function () {
        PersonalInfo.logout();
    });

    $('.header .login').click(function () {
        if(!$(this).find('.login_top').is(':visible')) {
            $('.shadow-popup').show();
            $('.popups > .popup-login-contanier').show();
        }
    });

});