<?php

use Bitrix\Main\Loader;

class PortalPersonalInfo extends CBitrixComponent {

    public function executeComponent() {
        $this->arResult['IS_USER'] = CUser::IsAuthorized();

        if (empty($this->arParams['IS_AJAX'])) {
            if(Loader::includeModule('igromafia.game')) {
                $this->arResult['USER'] = \Igromafia\Game\User::getInfo();
            }
            $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
            $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
            $this->includeComponentTemplate();
        } else {
            if ($this->arResult['IS_USER'] and $this->arParams['METHOD'] === 'LOGOUT') {
                global $USER;
                $USER->Logout();
            }
        }
    }

}
