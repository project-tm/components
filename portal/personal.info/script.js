(function (window) {

    if (window.jsPortalPersonalInfo) {
        return;
    }

    window.jsPortalPersonalInfo = function (arParams) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX
        };
        self.logout = function () {
            $.get(self.config.ajax, function (data) {
                window.location.reload();
            }, 'json');
        };
    };
})(window);