<?

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
$arResult['isOk'] = $APPLICATION->IncludeComponent("portal:favorites.ajax", $_REQUEST["TEMPLATE_NAME"], Array(
    "ADD" => $_REQUEST["ADD"],
    "TYPE" => $_REQUEST["TYPE"],
    "ELEMENT_ID" => (int)$_REQUEST["ELEMENT_ID"],
        ));
ob_get_clean();
echo json_encode($arResult);
