<?php

use Bitrix\Main\Loader,
    Igromafia\Game\Model\FavoritTable,
    Bitrix\Highloadblock as HL;

class PortalFavorites extends CBitrixComponent {

    public function executeComponent() {
        if (Loader::includeModule('igromafia.game')) {
            $this->arParams['IBLOCK_ID'] = Igromafia\Game\Favorit::getIblock($this->arParams['TYPE']);
            if (CUser::IsAuthorized()) {
                $userId = $GLOBALS['USER']->GetID();
                $rsData = Igromafia\Game\Model\FavoritTable::getList(array(
                            'select' => array('ID'),
                            'filter' => array(
                                'UF_ENTITY' => $this->arParams['TYPE'],
                                'UF_USER' => $userId,
                                'UF_ID' => $this->arParams['ELEMENT_ID']
                            ),
                ));
                $rsData = new CDBResult($rsData);
                $this->arResult['INFAVORITES'] = $rsData->Fetch();

                if (isset($_REQUEST['ADD'])) {
                    if ((int) $_REQUEST['ADD']) {
                        if (!$this->arResult['INFAVORITES']) {
                            if (!FavoritTable::add(array(
                                        'UF_ENTITY' => $this->arParams['TYPE'],
                                        'UF_XML_ID' => $this->arParams['IBLOCK_ID'],
                                        'UF_USER' => $userId,
                                        'UF_ID' => $this->arParams['ELEMENT_ID']
                                    )))
                                throw new Exception("ERR");
                        }
                    }else {
                        if ($this->arResult['INFAVORITES']) {
                            FavoritTable::Delete($this->arResult['INFAVORITES']['ID']);
                        }
                    }
                    return true;
                }
            }
            $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
            $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
            $this->includeComponentTemplate();
        }
    }

}
