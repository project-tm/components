<?
$jsParams = array(
    "AJAX" => $arResult["AJAX"],
    "TYPE" => $arParams["TYPE"],
    "SELECT" => $arParams["SELECT"],
    "ELEMENT_ID" => $arParams["ELEMENT_ID"],
    "INFAVORITES" => !empty($arResult['INFAVORITES'])
);
Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
?>
<script>
    var jsPortalСollectionGame = new jsPortalFavorites(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>,
            function (t) {
                $(t).toggleClass('added');
                if ($(t).hasClass('added')) {
                    $(t).text('В моей коллекции');
                } else {
                    $(t).text('Добавить в коллекцию игр');
                }
            }
    );
</script>