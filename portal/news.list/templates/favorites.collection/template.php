<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$count = 0;
?>
<? if ($arResult["ITEMS"]) { ?>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        if (empty($arItem)) {
            continue;
        }
        ?>
        <? $count++ ?>
        <div class="game-card toggle-js">
            <a class="icon-game" style="background:url('<?= $arItem['IMG'] ?>');"></a>
            <div class="float-wrap">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="name-game"><?= $arItem['NAME'] ?></a>
                <? $APPLICATION->IncludeComponent("portal:ajax.wrapper", "vote.item", array('PARAM' => $arItem['ID'])); ?>
            </div>
            <div class="float-wrap2">
                <div class="delete" data-id="<?= $arItem['ID'] ?>">убрать из игротеки</div>
                <? if (isset($arResult["DZHO"][$arItem['ID']])) { ?>
                    <a href="<?= $arResult["DZHO"][$arItem['ID']] ?>" class="for-sell-btn">продаю эту игру</a>
                <? } else { ?>
                    <a href="/lavka-dzho/add/?game=<?= $arItem['ID'] ?>&name=<?= urlencode($arItem['NAME']) ?>" class="sell-btn">выставить на продажу</a>
                <? } ?>
            </div>
            <div class="clear"></div>
        </div>
    <? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
<? } else { ?>
    <script>
    <? if ($_REQUEST['PAGEN_1'] > 2) { ?>
            window.location.href = '<?= $arParams['PAGER_BASE_LINK'] ?>?PAGEN_1=<?= $_REQUEST['PAGEN_1'] - 1 ?>';
    <? } elseif ($_REQUEST['PAGEN_1'] == 2) { ?>
                window.location.href = '<?= $arParams['PAGER_BASE_LINK'] ?>';
    <? } ?>
    </script>
<? } ?>
<? if (empty($count)) { ?>
    <div class="personal-list-empty">В игротеку ничего не добавлено</div>
<? } ?>
