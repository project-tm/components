<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$count = 0;
?>
<? if ($arResult["ITEMS"]) { ?>
    <div class="top-line favorites-section-contaner-filter">
        <? foreach ($arResult['FAVORITES'] as $key => $value) { ?>
            <a class="<? if (isset($value['SELECTED'])) { ?>active<? } ?> filter-item" data-filter="<?= $key ?>"><?= $value['NAME'] ?></a>
        <? } ?>
    </div>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <? $count++ ?>
        <div class="mid-content">
            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="categories <?= $arItem['TYPE']['class'] ?>"><?= $arItem['TYPE']['name'] ?></a> <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="name-game"><?= $arItem['NAME'] ?></a>
            <a class="remove-link" data-id="<?= $arItem['ID'] ?>" data-item="<?= strtoupper($arItem['TYPE']['class']) ?>">убрать</a>
        </div>
    <? endforeach; ?>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
<? } else { ?>
    <script>
    <? if ($_REQUEST['PAGEN_1'] > 2) { ?>
            window.location.href = '<?= $arParams['PAGER_BASE_LINK'] ?>?PAGEN_1=<?= $_REQUEST['PAGEN_1'] - 1 ?>';
    <? } elseif ($_REQUEST['PAGEN_1'] == 2) { ?>
                window.location.href = '<?= $arParams['PAGER_BASE_LINK'] ?>';
    <? } ?>
    </script>
<? } ?>
<? if (empty($count)) { ?>
    <div class="personal-list-empty">В избранное ничего не добавлено</div>
<? } ?>