<? if ($arParams['WRAPPER']['IS_FULL']) { ?>
    <div class="publication row <?= $arParams['WRAPPER']["CONTANER"] ?>" id="PUBLICATION">
        <div class="m40 public upp-text">публикации</div>
        <div class="<?= $arParams['WRAPPER']["CONTANER_LIST"] ?>">
        <? } ?>
        <? foreach ($arResult['PUBLICATION'] as $key => $item) { ?>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-pdg <?= $key % 2 ? 'public-wrap-block-right' : 'public-wrap-block-left' ?>">
                <div class="publication-block">
                    <span class="text">Публикация <?= $item['IBLOCK_CODE_NAME']; ?></span>
                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="link" title="<?= $item['NAME'] ?>"><?= $item['NAME'] ?></a>
                </div>
            </div>
        <? } ?>
        <script>
<? if ($arResult['PAGE_IS_NEXT']) { ?>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(true, 'class-hidden');
<? } else { ?>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(false, 'class-hidden');
<? } ?>
        </script>
        <? if ($arParams['WRAPPER']['IS_FULL']) { ?>
        </div>
        <div class="clear"></div>
        <div class="wrap_show-more">
            <div class="show-more mplavkadjo-show-more <?= $arParams['WRAPPER']["CONTANER_MORE"] ?>">Показать еще</div>
        </div>
    </div>
<? } ?>