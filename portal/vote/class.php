<?php

use Bitrix\Main\Loader,
    Igromafia\Game\Config,
    Project\Core\Utility,
    CIBlockElement;

class PortalVote extends CBitrixComponent {

    public function executeComponent() {
        if (Loader::includeModule('igromafia.game')) {
            $list = Utility::useCache([__CLASS__, __FUNCTION__], function() {
                        $arResult = array();
                        $arSelect = array('ID');
                        $arFilter = array(
                            'IBLOCK_ID' => Config::GAME_IBLOCK,
                            'ACTIVE' => 'Y',
                            '!DETAIL_PICTURE' => false
                        );
                        $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
                        while ($arItem = $res->Fetch()) {
                            $arResult[] = $arItem['ID'];
                        }
                        return $arResult;
                    });

            $this->arResult['ID'] = $list[array_rand($list)];
            $this->includeComponentTemplate();
        }
    }

}
