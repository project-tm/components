(function (window) {

    if (window.jsPortalFavoritesList) {
        return;
    }

    window.jsPortalFavoritesList = function (arParams) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX,
            select: arParams.SELECT,
            contaner: arParams.CONTANER,
            contanerFilter: arParams.CONTANER_FILTER
        };
        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            FILTER: arParams.FILTER,
            TYPE: arParams.TYPE,
            PAGEN_1: arParams.PAGEN
        };

        $(document).on('click', self.config.select, function () {
            self.param.ITEM = $(this).data('item');
            if (!self.param.ITEM) {
                self.param.ITEM = self.param.TYPE;
            }
            self.param.ELEMENT_ID = $(this).data('id');
            $.get(self.config.ajax, self.param, function (data) {
                $(self.config.contaner).html(data.content);
            }, 'json');
        });

        $(document).on('click', self.config.contanerFilter, function () {
            let param = $(this).data('filter');
            if (self.param.FILTER != param) {
                $(self.config.contanerFilter).each(function () {
                    if (param == $(this).data('filter')) {
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });
                self.param.FILTER = param;
                $.get(self.config.ajax, self.param, function (data) {
                    $(self.config.contaner).html(data.content);
                }, 'json');
            }
        });

    };
})(window);