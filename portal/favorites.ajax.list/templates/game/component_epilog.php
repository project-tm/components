<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "PAGEN" => $arResult["PAGEN"],
        "FILTER" => $arParams["FILTER"],
        "TYPE" => $arParams["TYPE"],
        "SELECT" => '.favorites-section-contaner .remove-link',
        "CONTANER" => '.favorites-section-contaner',
        "CONTANER_FILTER" => '.favorites-section-contaner-filter .filter-item',
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        jsPortalFavoritesList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>