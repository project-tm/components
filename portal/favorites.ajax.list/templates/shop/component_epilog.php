<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "PAGEN" => $arResult["PAGEN"],
        "TYPE" => $arParams["TYPE"],
        "SELECT" => '.games-section-item .delete-game',
        "CONTANER" => '.games-section-contaner',
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        jsPortalFavoritesList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>