<?php

$arParams = array(
    'IBLOCK_TYPE' => 'content',
    'IBLOCK_ID' => '5',
    'SECTION_ID' => NULL,
    'FILTER_NAME' => 'arrFilter',
    'PRICE_CODE' =>
    array(
    ),
    'CACHE_TYPE' => 'A',
    'CACHE_TIME' => '36000000',
    'CACHE_GROUPS' => 'Y',
    'SAVE_IN_SESSION' => 'N',
    'FILTER_VIEW_MODE' => 'HORIZONTAL',
    'XML_EXPORT' => 'Y',
    'SECTION_TITLE' => 'NAME',
    'SECTION_DESCRIPTION' => 'DESCRIPTION',
    'HIDE_NOT_AVAILABLE' => 'N',
    'TEMPLATE_THEME' => 'black',
    'CONVERT_CURRENCY' => 'N',
    'CURRENCY_ID' => NULL,
    'SEF_MODE' => 'Y',
    'SEF_RULE' => '/games/#SECTION_CODE_PATH#/filter/#SMART_FILTER_PATH#/apply/',
    'SMART_FILTER_PATH' => 'genres-is-qinqsm4i',
    'PAGER_PARAMS_NAME' => NULL,
    'INSTANT_RELOAD' => 'Y',
);
ob_start();
$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", ".default", $arParams);
ob_get_clean();
