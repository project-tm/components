<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="row catalog article-section-wrapper">
    <? } ?>
    <? if ($arResult['ITEMS']) { ?>
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <div class="block_article">
            <a class="art-img" style="background: url('<?= $arItem['IMG'] ?>') no-repeat;" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"></a>
            <div class="art-body">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="art-name">
                    <?= $arItem['NAME'] ?>
                </a>
                <? if (isset($arResult['USER'][$arItem['CREATED_BY']])) { ?>
                    <div class="autor-name">
                        автор: <a href="<?= $arResult['USER'][$arItem['CREATED_BY']]['URL'] ?>"><?= $arResult['USER'][$arItem['CREATED_BY']]['NAME'] ?></a>
                    </div>
                <? } ?>
                <div class="art-txt">
                    <?= $arItem['PREVIEW_TEXT'] ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    <? } ?>
    <? } else { ?>
        <div class="personal-list-empty">По этой игре статей нет, вы можете быть первым</div>
    <? } ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
    <? if ($arResult['PAGE_IS_NEXT']) { ?>
        <div class="wrap_show-more">
            <div class="show-more article-section-show-more">Еще статьи</div>
        </div>
    <? } ?>
<? } ?>