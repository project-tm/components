<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "CONTANER" => '.article-section-wrapper',
        "CONTANER_MORE" => '.article-section-show-more',
        "PAGEN_1" => $arResult["PAGE_NEXT"],
        "SECTION_ID" => (int) $arParams["SECTION_ID"],
        "COUNT" => (int) $arParams["COUNT"],
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        new jsPortalGameSection(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>