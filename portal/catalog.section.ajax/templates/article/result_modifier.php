<?

$userId = array();
foreach ($arResult['ITEMS'] as &$arItem) {
    $userId[$arItem['CREATED_BY']] = $arItem['CREATED_BY'];
    $img = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 226, 140);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH .'/assets/images/hz.png';
    }
}
unset($arItem);

$arResult['USER'] = Igromafia\Game\User::getList($userId);
