<?

foreach ($arResult['ITEMS'] as &$arItem) {
    $userId[$arItem['CREATED_BY']] = $arItem['CREATED_BY'];
    $img = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 323, 180);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH .'/games/games3.png';
    }

    $arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
    $arResult['PLATFORMS'] = array();
    if (!empty($arResult['PROPERTIES']['PLATFORM']['VALUE'])) {
        foreach ($arResult['PROPERTIES']['PLATFORM']['VALUE'] as $key => $value) {
            $arResult['PLATFORMS'][$key] = Igromafia\Game\Property::getPlatform($value);
        }
    }
    $arItem['COUNTRY'] = Igromafia\Game\Property::getCountry($arItem['PROPERTIES']['COUNTRY']['VALUE']);
}
unset($arItem);