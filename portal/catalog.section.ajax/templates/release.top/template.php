<h2 class="">ожидаемые релизы</h2>
<div class="next-release-js release-top-section-wrapper">
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <div class="main-release-block">
            <a class="release-image" href="">
                <div class="wrap_release-image"style="background: url('<?= $arItem['IMG'] ?>') no-repeat;"></div>
            </a>
            <div class="release-info">
                <div class="data"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></div>
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="name">Call of Duty: Black Ops Ops Ops</a>
                <? if ($arItem['PLATFORMS']) { ?>
                    <div class="release-info_platforms">Платформа
                        <? foreach ($arItem['PLATFORMS'] as $value) { ?>
                            <span class="console"><?= $value ?></span>
                        <? } ?>
                    </div>
                <? } ?>
                <? if ($arItem['COUNTRY']) { ?>
                    <div class="country">Где: <span><?= $arItem['COUNTRY'] ?></span></div>
                <? } ?>
            </div>
        </div>
    <? } ?>
</div>
