$(function () {
    'use strict';

    $(document).on('click', '.section-mafia-rating .block-star:not(.rating-disabled), .section-user-rating .numbers-block:not(.rating-disabled)', function () {
        if ($('.smartfilter .filter-rating').val() == $(this).text()) {
            $('.smartfilter .filter-rating').val('');
        } else {
            $('.smartfilter .filter-rating').val($(this).text());
        }
        $('.smartfilter').submit();
    });

});