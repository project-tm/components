<?

foreach ($arResult['ITEMS'] as &$arItem) {
    if (!empty($arItem['PROPERTY_PLATFORM_VALUE'])) {
        foreach ($arItem['PROPERTY_PLATFORM_VALUE'] as $key => $value) {
            $name = Igromafia\Game\Property::getPlatform($value);
            if($name) {
                $arItem['PROPERTY_PLATFORM_VALUE'][$key] = $name;
            } else {
                unset($arItem['PROPERTY_PLATFORM_VALUE'][$key]);
            }
        }
    }
    $arItem['RATING_MAFIA'] = $arItem['PROPERTY_RATINGIGROMAFII_VALUE'];
    $arItem['RATING_USER'] = round($arItem['PROPERTY_RATING_VALUE'], 1);

    $img = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 300, 213);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH .'/assets/images/hz.png';
    }
}
unset($arItem);

if (empty($arResult['IS_AJAX'])) {
    $type = $arParams['RATING_MAFIA'] ? 'PROPERTY_RATINGIGROMAFII' :  'PROPERTY_RATING';
    $arResult['RATING'] = array();
    unset($arResult['FILTER']['>PROPERTY_RATINGIGROMAFII'], $arResult['FILTER']['>PROPERTY_RATING']);
    unset($arResult['FILTER']['<=PROPERTY_RATINGIGROMAFII'], $arResult['FILTER']['<=PROPERTY_RATING']);
    $res = CIBlockElement::GetList(array(), $arResult['FILTER'], array($type), false, array($type));
    while ($arItem = $res->GetNext()) {
        $arResult['RATING'][ceil($arItem[$type . '_VALUE'])] = true;
    }

	$this->getComponent()->setResultCacheKeys(array(
		"RATING",
	));
}