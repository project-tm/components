<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="wrapper">
        <div class="row catalog game-section-wrapper">
        <? } ?>
        <? foreach ($arResult['ITEMS'] as $arItem) { ?>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 section-game-item">
                <a class="cover gm1" style="background: url('<?= $arItem['IMG'] ?>') no-repeat;" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                    <? if ($arItem['PROPERTY_PLATFORM_VALUE']) { ?>
                        <div class="console-wrap">
                            <? foreach ($arItem['PROPERTY_PLATFORM_VALUE'] as $value) { ?>
                                <div class="console"><?= $value ?></div>
                            <? } ?>
                        </div>
                    <? } ?>
                    <? if(empty($arParams['RATING_MAFIA']) and $arItem['RATING_USER']) { ?>
                        <div class="rating"><?=$arItem['RATING_USER'] ?></div>
                    <? } ?>
                </a>
                <div class="reviews_bot">
                    <a class="game" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                </div>
                 <? if($arParams['RATING_MAFIA'] and $arItem['RATING_MAFIA']) { ?>
                    <div class="bg-for-star">
                        <a class="star" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span class="number"><?= $arItem['RATING_MAFIA'] ?></span></a>
                    </div>
                <? } ?>
            </div>
        <? } ?>
        <? if (empty($arResult['IS_AJAX'])) { ?>
        </div>
        <? if ($arResult['PAGE_IS_NEXT']) { ?>
            <div class="wrap_show-more">
                <div class="show-more game-section-show-more">Показать еще </div>
            </div>
        <? } ?>
    </div>
<? } ?>