<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="imp-wrap news-section-wrapper">
    <? } ?>
    <? if ($arResult['ITEMS']) { ?>
        <? foreach ($arResult['ITEMS'] as $arItem) { ?>
            <div class="block_new row">
                <a style="background: url('<?= $arItem['IMG'] ?>') no-repeat;" href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="art-img col-lg-4 col-md-4 col-sm-4 col-xs-3"></a>
                <div class="art-body col-lg-8 col-md-8 col-sm-8 col-xs-9">
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="art-name">
                        <?= $arItem['NAME'] ?>
                    </a>
                    <? if (isset($arResult['USER'][$arItem['CREATED_BY']])) { ?>
                        <div class="autor-name">
                            автор: <a href="<?= $arResult['USER'][$arItem['CREATED_BY']]['URL'] ?>"><?= $arResult['USER'][$arItem['CREATED_BY']]['NAME'] ?></a>
                        </div>
                    <? } ?>
                    <div class="art-txt">
                        <?= $arItem['PREVIEW_TEXT'] ?>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        <? } ?>
    <? } else { ?>
        <div class="personal-list-empty">По этой игре новостей нет, вы можете быть первым</div>
    <? } ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
    <div class="clear"></div>
    <? if ($arResult['PAGE_IS_NEXT']) { ?>
        <div class="wrap_show-more">
            <div class="show-more news-section-show-more">Еще новости</div>
        </div>
    <? } ?>
<? } ?>