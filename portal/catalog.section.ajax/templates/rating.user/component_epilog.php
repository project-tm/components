<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "CONTANER" => '.game-section-user-wrapper',
        "CONTANER_RATING" => '.game-section-user-number div',
        "CONTANER_MORE" => '.game-section-user-show-more',
        "RATING" => $arParams["RATING"],
        "PAGEN_1" => $arResult["PAGE_NEXT"],
        "COUNT" => (int) $arParams["COUNT"],
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        var jsRatingUserSection = new jsPortalGameSection(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        jsRatingUserSection.config.reloadRating = function () {
            console.log(5);
            $('.wrap-game-js').slick(getWrapGameSlickConfig());
        };
    </script>
<? } ?>