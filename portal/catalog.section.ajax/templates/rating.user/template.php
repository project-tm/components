<? if (empty($arResult['IS_AJAX'])) { ?>
    </div>
    <div class="game2-white-bg">
        <div class="wrapper">
            <h2 class="rating-big-txt"><span class="sry_crutch">рейтинг пользователей</span></h2>
            <div class="number game-section-user-number">
                <? for ($i=1; $i<=10; $i++) { ?>
                    <div class="numbers-block section-rating-number <? if(empty($arResult['RATING'][$i])) { ?>rating-disabled<? } ?>"><?= $i ?></div>
                <? } ?>
            </div>
            <div class="game-section-user-wrapper">
            <? } ?>
            <div class="wrap-game-js">
                <? foreach ($arResult['ITEMS'] as $arItem) { ?>
                    <div class="game-slider2">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="wrap-img" style="background: url('<?= $arItem['IMG'] ?>') no-repeat">
                            <? if ($arItem['RATING_USER']) { ?>
                                <div class="rating"><?= $arItem['RATING_USER'] ?></div>
                            <? } ?>
                            <? if ($arItem['PROPERTY_PLATFORM_VALUE']) { ?>
                                <div class="console-wrap">
                                    <? foreach ($arItem['PROPERTY_PLATFORM_VALUE'] as $value) { ?>
                                        <div class="console"><?= $value ?></div>
                                    <? } ?>
                                </div>
                            <? } ?>
                        </a>
                        <div class="name"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></div>
                    </div>
                <? } ?>
            </div>
            <? if (empty($arResult['IS_AJAX'])) { ?>
            </div>
        </div>
    </div>
<? } ?>