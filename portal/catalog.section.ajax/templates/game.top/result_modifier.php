<?

foreach ($arResult['ITEMS'] as &$arItem) {
    if (!empty($arItem['PROPERTY_PLATFORM_VALUE'])) {
        foreach ($arItem['PROPERTY_PLATFORM_VALUE'] as $key => $value) {
            $name = Igromafia\Game\Property::getPlatform($value);
            if($name) {
                $arItem['PROPERTY_PLATFORM_VALUE'][$key] = $name;
            } else {
                unset($arItem['PROPERTY_PLATFORM_VALUE'][$key]);
            }
        }
    }

    $img = empty($arItem['PREVIEW_PICTURE']) ? $arItem['DETAIL_PICTURE'] : $arItem['PREVIEW_PICTURE'];
    if ($img) {
        $arItem['IMG'] = Igromafia\Game\Image::resize($img, 300, 215);
    } else {
        $arItem['IMG'] = SITE_TEMPLATE_PATH .'/assets/images/hz.png';
    }
}
unset($arItem);