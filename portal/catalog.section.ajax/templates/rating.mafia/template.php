<? if (empty($arResult['IS_AJAX'])) { ?>
    <div class="wrapper">
        <div class="mafia-reting">
            <div class="h2 rating-big-txt">рейтинг ИГРОМАФИИ</div>
            <div class="rat-star game-section-mafia-number">
                <? for ($i=1; $i<=10; $i++) { ?>
                    <div class="block-star section-rating-number <? if(empty($arResult['RATING'][$i])) { ?>rating-disabled<? } ?>"><?= $i ?></div>
                <? } ?>
            </div>
        </div>
        <div class="row catalog game-section-mafia-wrapper">
        <? } ?>
        <? foreach ($arResult['ITEMS'] as $arItem) { ?>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="cover gm1" style="background: url('<?= $arItem['IMG'] ?>') no-repeat"></a>
                <div class="reviews_bot">
                    <a class="game" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                </div>
                <? if ($arItem['RATING_MAFIA']) { ?>
                    <div class="bg-for-star">
                        <a class="star" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><span class="number"><?= $arItem['RATING_MAFIA'] ?></span></a>
                    </div>
                <? } ?>
            </div>
        <? } ?>
        <? if (empty($arResult['IS_AJAX'])) { ?>
        </div>
        <? if ($arResult['PAGE_IS_NEXT']) { ?>
            <div class="wrap_show-more">
                <div class="show-more game-section-mafia-show-more">Показать еще </div>
            </div>
        <? } ?>
    <!--</div>-->
<? } ?>