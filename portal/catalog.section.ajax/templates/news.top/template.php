<h2>НОВОСТИ / <span class="small-text">СТАТЬИ</span></h2>
<div class="news-block_slider">
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="slide news-block__item">
            <div class="wrap-img"><div class="img" style="background: url('<?= $arItem['IMG'] ?>') no-repeat;"></div></div>
            <div class="right-content">
                <div class="anons"><?= $arItem['NAME'] ?></div>
                <div class="all-text"><?= TruncateText(strip_tags($arItem['PREVIEW_TEXT']), 300) ?></div>
            </div>
            <div class="clear"></div>
        </a>
    <? } ?>
</div>