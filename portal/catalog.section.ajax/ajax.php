<?

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
$arResult['isNext'] = $APPLICATION->IncludeComponent("portal:catalog.section.ajax", $_REQUEST["TEMPLATE_NAME"], Array(
    "IS_AJAX" => 'Y',
    "IBLOCK_ID" => $_REQUEST["IBLOCK_ID"],
    "PAGE" => $_REQUEST["PAGEN_1"],
    "RATING" => $_REQUEST["RATING"],
    "PARAM" => $_REQUEST["PARAM"],
    "FILTER_RATING" => $_REQUEST["FILTER_RATING"],
    "SECTION_ID" => (int) $_REQUEST["SECTION_ID"],
    "COUNT" => (int) $_REQUEST["COUNT"],
        ));
$arResult['content'] = ob_get_clean();
echo json_encode($arResult);
