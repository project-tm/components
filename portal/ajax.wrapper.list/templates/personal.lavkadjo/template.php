<? if ($arResult['IS_SHOP']) { ?>
    <? if (empty($arResult['IS_AJAX'])) { ?>
        <div class="<?= $arResult["CONTANER"] ?>">
        <? } ?>
        <?
        global $arrFilterUser;
        $arrFilterUser = array(
            'PROPERTY_SELLER' => cUser::getId(),
            'ACTIVE' => array('Y', 'N'),
            'ACTIVE_DATE' => array('Y', 'N')
        );
        $APPLICATION->IncludeComponent(
                "bitrix:news.list", "personal.lavkadjo", Array(
            "WRAPPER" => $arResult,
            "IS_EDIT" => 'Y',
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => 14,
            "NEWS_COUNT" => 4,
            "SORT_BY1" => "TIMESTAMP_X",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "DESC",
            "FIELD_CODE" => array('DETAIL_PICTURE'),
            "PROPERTY_CODE" => array("CITY", "GAME_FROM_PORTAL", "EXCHANGE", "PRICE", "PLATFORM",),
            "DETAIL_URL" => "/lavka-dzho/#ID#/",
            "SECTION_URL" => "",
            "IBLOCK_URL" => "",
            "DISPLAY_PANEL" => "N",
            "SET_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "MESSAGE_404" => "N",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "FILE_404" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => 36000,
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "N",
            "PAGER_TEMPLATE" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "N",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_BASE_LINK" => "N",
            "PAGER_PARAMS_NAME" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "PREVIEW_TRUNCATE_LEN" => "N",
            "ACTIVE_DATE_FORMAT" => "N",
            "USE_PERMISSIONS" => "N",
            "GROUP_PERMISSIONS" => "N",
            "FILTER_NAME" => "arrFilterUser",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "CHECK_DATES" => "Y",
            'cGetArticle' => false
                ), false
        );
        ?>
        <? if ($arResult['IS_FULL']) { ?>
        </div>
        <div class="wrap_show-more">
            <div class="show-more mplavkadjo-show-more <?= $arResult["CONTANER_MORE"] ?>">Показать еще</div>
        </div>
    <? } ?>
<? } ?>