<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset;

class PortalAjaxWrapperList extends CBitrixComponent {

    private function startWrapperTemplate() {
        if (empty($this->arResult['IS_FULL'])) {
            return;
        }
        Asset::getInstance()->addJs($this->arResult["SCRIPT"]);
        $jsParams = array(
            "AJAX" => $this->arResult["AJAX"],
            "TEMPLATE_NAME" => $this->arResult["TEMPLATE_NAME"],
            "PARAM" => $this->arResult["PARAM"],
            "CONTANER" => '.' . $this->arResult["CONTANER"],
            "CONTANER_LIST" => '.' . $this->arResult["CONTANER_LIST"],
            "CONTANER_MORE" => '.' . $this->arResult["CONTANER_MORE"],
            "CONTANER_FILTER" => '.' . $this->arResult["CONTANER_FILTER"],
        );
        ?>
        <script>
            var <?= $this->arResult['JS_OBJECT'] ?> = new jsPortalAjaxWrapperList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
        $this->arResult['IS_FULL'] = $this->arParams['IS_UPDATE'] ?: empty($this->arResult['IS_AJAX']);
        $this->arResult['PAGEN'] = ceil($this->arParams['PAGEN'] ?: 1);
        $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
        $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
        $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
        $this->arResult["FILTER"] = $this->arParams["FILTER"] ?: '';
        $this->arResult["PARAM"] = $this->arParams["PARAM"] ?: '';
        $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arResult["PARAM"] ?: '');

        $this->arResult['CONTANER'] = 'ajax-wrapper-list-contaner' . $key;
        $this->arResult['CONTANER_LIST'] = 'ajax-wrapper-list' . $key;
        $this->arResult['CONTANER_MORE'] = 'ajax-wrapper-list-more' . $key;
        $this->arResult['CONTANER_FILTER'] = 'ajax-wrapper-list-filter' . $key;
        $this->arResult['JS_OBJECT'] = 'jsWrapperList' . $key;
        if (empty($this->arResult['IS_AJAX'])) {
            Asset::getInstance()->addJs($this->arResult["SCRIPT"]);
            $this->startWrapperTemplate();
        }
        $this->includeComponentTemplate();
    }

}
