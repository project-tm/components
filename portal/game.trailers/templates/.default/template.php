<? if ($arParams['WRAPPER']['IS_FULL']) { ?>
    <div class="trailers <?= $arParams['WRAPPER']["CONTANER"] ?>">
        <div class="top-txt">ПОПУЛЯРНЫЕ ТРЕЙЛЕРЫ</div>
        <div class="<?= $arParams['WRAPPER']["CONTANER_LIST"] ?>">
        <? } ?>
        <? foreach ($arResult['TRAILERS'] as $arItem) { ?>
            <div class="video">
                <iframe width="100%" src="<?= $arItem['PROPERTY_TRAILERS_VALUE'] ?>" frameborder="0" allowfullscreen=""></iframe>
            </div>
            <div class="logo-and-name">
                <div class="logo-ut"><?= $arItem['NAME'] ?></div>
            </div>
        <? } ?>
        <script>
<? if ($arResult['PAGE_IS_NEXT']) { ?>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(true, 'hidden');
<? } else { ?>
    <?= $arParams['WRAPPER']['JS_OBJECT'] ?>.showNext(false, 'hidden');
<? } ?>
        </script>
        <? if ($arParams['WRAPPER']['IS_FULL']) { ?>
        </div>
        <div class="wrap_show-more-comments">
            <div class="show-more-comments <?= $arParams['WRAPPER']["CONTANER_MORE"] ?>">Показать еще </div>
        </div>
    </div>
<? } ?>