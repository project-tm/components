(function (window) {

    if (window.jsPortalAjaxWrapper) {
        return;
    }

    window.jsPortalAjaxWrapper = function (arParams) {
        var self = this;
        var config = {
            ajax: arParams.AJAX,
            formSelect: 'form.' + arParams.FORM_SELECT,
            contaner: '#' + arParams.CONTANER,
        };
        var param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            PARAM: arParams.PARAM
        };

        if (arParams.FORM_SELECT && $(config.formSelect).is('form')) {
            console.log(config.formSelect);
            $(document).on('submit', config.formSelect, function () {
                self.loadAjax($(this).serialize());
                return false;
            });
        }

        self.loadAjax = function (data, func) {
            $.post(config.ajax + '?' + $.param(param), data, function (data) {
                if (data) {
                    $(config.contaner).html(data.content);
                    if (func) {
                        func(data);
                    }
                    if (self.handler) {
                        self.handler();
                    }
                }
            }, 'json');
        };
    };
})(window);