<?

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
$APPLICATION->IncludeComponent("portal:ajax.wrapper", $_REQUEST["TEMPLATE_NAME"], Array(
    "IS_AJAX" => 'Y',
    "PARAM" => $_REQUEST["PARAM"]
));
$arResult['content'] = ob_get_clean();
$arResult['IsAuthorized'] = CUser::IsAuthorized();
echo json_encode($arResult);
