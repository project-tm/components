<?

if (Bitrix\Main\Loader::includeModule('project.support')) {
    $arGame = Project\Support\JoShop::getById($arResult['PARAM']);
    $APPLICATION->IncludeComponent("project.support:form.message", '.default', array(
        'TYPE' => $arGame['SELLER_ID'],
        'GAME' => $arGame,
        'FORM_SELECT' => $arResult['FORM_SELECT']
    ));
}
