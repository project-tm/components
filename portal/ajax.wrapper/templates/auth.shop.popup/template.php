<?

if (!CUser::IsAuthorized()) {
    $APPLICATION->IncludeComponent(
            "bitrix:system.auth.form", "header.shop", array(
        "FORM_SELECT" => $arResult["FORM_SELECT"],
        "AUTH_RESULT" => $APPLICATION->arAuthResult,
        "REGISTER_URL" => SITE_DIR . "personal/?register=yes",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => SITE_DIR . "personal/",
        "SHOW_ERRORS" => "Y",
            ), false
    );
}
