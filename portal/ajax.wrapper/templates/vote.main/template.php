<?
$APPLICATION->IncludeComponent(
	"askaron:askaron.ibvote.iblock.vote",
	".default", 
	array(
		"FORM_SELECT" => $arResult["FORM_SELECT"],
		"CACHE_TIME" => "10",
		"CACHE_TYPE" => "A",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"COOKIE_CHECK" => "N",
		"ELEMENT_ID" => (int)$arResult["PARAM"],
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "content",
		"IP_CHECK_TIME" => "0",
		"MAX_VOTE" => "10",
		"SESSION_CHECK" => "N",
		"SET_STATUS_404" => "N",
		"USER_ID_CHECK_TIME" => "0",
		"VOTE_NAMES" => array(
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
			4 => "5",
			5 => "6",
			6 => "7",
			7 => "8",
			8 => "9",
			9 => "10"
		),
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);