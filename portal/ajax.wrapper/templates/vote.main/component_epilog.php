<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "PARAM" => $arResult["PARAM"],
        "FORM_SELECT" => $arResult["FORM_SELECT"],
        "CONTANER" => $arResult["CONTANER"],
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        var formVoteMain = new jsPortalAjaxWrapper(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>