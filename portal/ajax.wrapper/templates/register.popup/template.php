<?

if (!CUser::IsAuthorized()) {
    $APPLICATION->IncludeComponent(
            "bitrix:system.auth.registration", "header", array(
//        "FORM_SELECT" => $arResult["FORM_SELECT"],
        "AUTH_RESULT" => $APPLICATION->arAuthResult,
        "URL_SHOP_RULES" => SITE_DIR . "about/",
        "EMPTY" => $arParams["EMPTY"],
        "COMPONENT_TEMPLATE" => "header"
            ), false
    );
}
