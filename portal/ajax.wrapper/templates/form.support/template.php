<?

if (Bitrix\Main\Loader::includeModule('project.support')) {
    $APPLICATION->IncludeComponent("project.support:form.support", $arParams["PARAM"], array(
        'THEME' => Project\Support\Theme::getHeader($arResult['PARAM']),
        'TYPE' => $arResult['PARAM'],
        'FORM_SELECT' => $arResult['FORM_SELECT']
    ));
}