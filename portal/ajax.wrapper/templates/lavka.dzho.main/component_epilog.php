<? if (empty($arResult['IS_AJAX']) and !CUser::IsAuthorized()) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "PARAM" => $arResult["PARAM"],
        "FORM_SELECT" => $arResult["FORM_SELECT"],
        "CONTANER" => $arResult["CONTANER"],
    );
    Bitrix\Main\Page\Asset::getInstance()->addJs($arResult["SCRIPT"]);
    ?>
    <script>
        var jsPortalAuthPopup = new jsPortalAjaxWrapper(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>