<?php

use Bitrix\Main\Loader,
    Bitrix\Main\SystemException;

class PortalAjaxWrapper extends CBitrixComponent {

    public function executeComponent() {
        $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
        $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
        $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
        $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();

        $this->arResult["PARAM"] = $this->arParams["PARAM"] ?: '';
        $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arResult["PARAM"] ?: '');

        $this->arResult['CONTANER'] = 'ajax-wrapper' . $key;
        $this->arResult['FORM_SELECT'] = 'ajax-' . str_replace('.', '-', $this->GetTemplateName() . $key);
        if (empty($this->arResult['IS_AJAX'])) {
            ?><div id="<?= $this->arResult['CONTANER'] ?>"><?
            }
            $this->includeComponentTemplate();
            if (empty($this->arResult['IS_AJAX'])) {
                ?></div><?
        }
    }

}
